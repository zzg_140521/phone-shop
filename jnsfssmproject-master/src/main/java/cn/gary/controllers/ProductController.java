package cn.gary.controllers;

import cn.gary.models.Goods;
import cn.gary.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/")
public class ProductController {
    @Autowired
    GoodsService goodsService;

    @GetMapping("/productlist")
    public String list(Integer pageIndex, Integer pageSize, Model model){
//        if(pageIndex==null){
//            pageIndex = 1;
//        }
//        if(pageSize == null){
//            pageSize = 10;
//        }
//
//        //加载数据
//        List<Goods> items = goodsService.list(pageIndex, pageSize);
//
//        model.addAttribute("items", items);

        //加载视图
        return "productlist.html";
    }


    @GetMapping("/productdetail")
    public String productdetail(Integer pid, Model model){

//        Goods product = goodsService.detail(pid);
//        model.addAttribute("product", product);

        return "productdetail";}
}
