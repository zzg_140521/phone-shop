package cn.gary.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * 前台
 */
@Controller
@RequestMapping("/")
public class IndexController {

    //@RequestMapping("/index")
    //@RequestMapping(value = "/index", method = RequestMethod.GET)
    @GetMapping("index")
    //@ResponseBody
    public String index(Model model){

        model.addAttribute("data", 100);

        return "index";
    }

    @RequestMapping("/news")
    public String news(){
        //加载视图
        return "newslist";
    }

    //@RequestMapping(path = "/upload", method = RequestMethod.POST)
    @PostMapping("/upload")
    public String upload(){
        //加载视图
        return "newslist";
    }

    @RequestMapping("/aboutus")
    public String aboutus(){
        //加载视图
        return "aboutus";
    }
}
