package cn.gary.controllers;

import cn.gary.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;

@Controller
@RequestMapping("/")
public class UserController {

    @Autowired
    UsersService usersService;

    @GetMapping("/login")
    public String login(HttpServletRequest request, Model model){
//        //获取之前存储的Cookie数据
//        Cookie[] cookies = request.getCookies();
//        String username = "";
//        for (Cookie cookie:cookies) {
//            if(cookie.getName().equals("username")){
//                username = cookie.getValue();
//            }
//        }
//        model.addAttribute("username", username);
        return "login";
    }

    @PostMapping("/login")
    public String login(String username, String userpwd, String checkbox1, Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session){
//        if(checkbox1 !=null && checkbox1.equals("1")){
//            //记住
//            //获取之前存储的Cookie数据
//            //Cookie[] cookies = request.getCookies();
//            //向Cookie中，写入数据
//            Cookie saveUserNameCookie = new Cookie("username", username);
//            response.addCookie(saveUserNameCookie);
//        }
//
//
//        boolean result = usersService.login(username, userpwd);
//
//        if(result){
//            //用户合法
//            //记录用户状态
//            session.setAttribute("username", username);
//            session.setAttribute("loginTime", new Date());
//            session.setAttribute("loginIP", request.getRemoteAddr());
//
//            return "redirect:/index";
//        }else{
//            //用户非法
//            model.addAttribute("info", "用户名或密码错误，请检查...");
//            return "login";
//        }
        return "redirect:/index";
    }

    @GetMapping("register")
    public String register(){
        return "register";
    }

    @GetMapping("usercenter")
    public String userCenter(){
        return "usercenter";
    }

    @GetMapping("logout")
    public String logout(HttpSession session){
        //身份状态清理，与界面好像没什么关系
        //记录用户状态    Session指定删除
        session.removeAttribute("username");
        session.removeAttribute("loginTime");
        session.removeAttribute("loginIP");

        //批量清除
        //session.invalidate();

        //并不是所有的网页都需要视图
        return "redirect:login";
    }

}
