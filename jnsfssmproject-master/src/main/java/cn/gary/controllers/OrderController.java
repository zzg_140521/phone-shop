package cn.gary.controllers;

import cn.gary.models.Cart;
import cn.gary.models.Goods;
import cn.gary.models.Users;
import cn.gary.service.CartService;
import cn.gary.service.GoodsService;
import cn.gary.service.OrdersService;
import cn.gary.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.UUID;

@Controller
public class OrderController {

    @Autowired
    CartService cartService;
    @Autowired
    GoodsService goodsService;
    @Autowired
    UsersService usersService;
    @Autowired
    OrdersService ordersService;

    @GetMapping("/my_addgwc")
    public String my_addgwc(Integer pnum, Integer pid, HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws IOException {

        /*
        //权限判断，是否是合法用户
        if(session.getAttribute("username") == null){
            //非法用户
            response.setCharacterEncoding("utf-8");
            response.setContentType("text/html;charset='utf-8';");
            String scriptinfo = "<script>alert(\"未登录或登录超时，请重新登录...\");window.location.href='/index';</script>";
            //将数据直接发送到客户端浏览器
            PrintWriter out = response.getWriter();
            out.print(scriptinfo);
            return null;
        }*/

        //记录购买者、购买信息...
        //insert
//        Goods good = goodsService.detail(pid);
//
//        String username = session.getAttribute("username").toString();
//        Users user = usersService.findByUserName(username);
//
//        Cart cart = new Cart();
//        cart.setProductId(pid);
//        cart.setProductName(product.getProductName());
//        cart.setProductPrice(product.getProductMarketPrice());
//        cart.setCartAmount(pnum);
//        cart.setUserId(user.getUserId());          //想办法获取userId
                                    //获取username是简单，session中有username

//        cartService.insert(cart);

        return "my_addgwc";
    }

    @GetMapping("/my_gwc1")
    public String my_gwc1(HttpServletRequest request, HttpSession session, Model model){
//        //获取当前购买者根据userId
//        String username = session.getAttribute("username").toString();
//        Users user = usersService.findByUserName(username);
//        //加载购物数据
//        List<Cart> items = cartService.findByUserId(user.getUserId());
//
//        //将数据送往View视图，完成呈现工作
//        //如何将Controller发送给View  将当前操作者的购物数据，送至View
//        model.addAttribute("cartItems", items);
//
//        //View中，还需要一个数据，总金额
//        Double total = cartService.totalByUserId(user.getUserId());
//        model.addAttribute("total", total);
        return "my_gwc1";
    }
    @GetMapping("/my_gwc2")
    public String my_gwc2(HttpServletRequest request, HttpSession session, Model model){

//        //也要向View送两个数据：1、购物车记录、2购物车总金额
//        //获取当前购买者根据userId
//        String username = session.getAttribute("username").toString();
//        Users user = usersService.findByUserName(username);
//        //加载购物数据
//        List<Cart> items = cartService.findByUserId(user.getUserId());
//        Double total = cartService.totalByUserId(user.getUserId());
//
//        model.addAttribute("cartItems", items);
//        model.addAttribute("total", total);


        return "my_gwc2";
    }

    //@GetMapping("/my_gwc3")
    @PostMapping("/my_gwc3")
    //@RequestMapping(value = "/my_gwc3", method= RequestMethod.POST)
    public ModelAndView my_gwc3(String OrderDeliverName, String OrderDeliverAddress, String OrderDeliverZipCode, String OrderDeliverMobile, HttpSession session, HttpServletResponse response, ModelAndView mav) throws IOException {
//        //如下都是操作数据库（service、dao）
//        //查询当前操作者的购物记录与购物总金额
//        String username = session.getAttribute("username").toString();
//        Users user = usersService.findByUserName(username);
//        String orderId = UUID.randomUUID().toString().replaceAll("-", "");
//        Double orderTotal = cartService.totalByUserId(user.getUserId());
//
//        //生成 订单表中的订单记录
//        //生成 订单明细表，将购物记录复制到订单明细表中
//        //清除购物表中的购物记录
//        //以上处理，肯定一次次dao方法调用，能不能不组合成一个组合拳
//        //搞成组合拳，也能方便的添加事务
//        try{
//            ordersService.order(user, orderId, OrderDeliverName, OrderDeliverAddress, OrderDeliverZipCode, OrderDeliverMobile);
//        }catch (Exception e){
//            //下单失败，提示重新下单
//            //指定编译时的字符集
//            response.setCharacterEncoding("utf-8");
//            //通知客户端浏览器，发给你的是什么东西
//            response.setContentType("text/html;charset=utf-8");
//            String scriptinfo = "<script>alert(\"下单失败，提示重新下单...\");window.location.href='/my_gwc2';</script>";
//            //将数据直接发送到客户端浏览器
//            PrintWriter out = response.getWriter();
//            out.print(scriptinfo);
//        }
//
//        //Model、ModelAndView
//        mav.addObject("orderId", orderId);
//        mav.addObject("orderTotal", orderTotal);
//        mav.setViewName("my_gwc3");
        return mav;
        //return "my_gwc3";
    }

}
