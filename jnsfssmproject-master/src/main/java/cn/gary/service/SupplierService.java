package cn.gary.service;

import cn.gary.dao.SupplierDao;
import cn.gary.models.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//供货商信息表

@Service(value = "supplierService")
public class SupplierService {

    @Autowired
    private SupplierDao supplierDao;

    public int deleteByPrimaryKey(Integer id) {return supplierDao.deleteByPrimaryKey(id);}

    public int insert(Supplier supplier){return supplierDao.insert(supplier);}

    public Supplier selectByPrimaryKey(Integer id){return supplierDao.selectByPrimaryKey(id);}

    public int updateByPrimaryKey(Supplier supplier){return supplierDao.updateByPrimaryKey(supplier);}

    public List<Supplier> selectAll(){return supplierDao.selectAll();}

}
