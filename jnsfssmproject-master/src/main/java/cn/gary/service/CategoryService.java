package cn.gary.service;

import cn.gary.dao.CategoryDao;
import cn.gary.models.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//商品类别信息表

@Service(value = "categoryService")
public class CategoryService {

    @Autowired
    private CategoryDao categoryDao;

    public int deleteByPrimaryKey(Integer id){return categoryDao.deleteByPrimaryKey(id);}

    public int insert(Category category){return categoryDao.insert(category);}

    public Category selectByPrimaryKey(Integer id){return categoryDao.selectByPrimaryKey(id);}

    public int updateByPrimaryKey(Category category){return categoryDao.updateByPrimaryKey(category);}

    public List<Category> selectAll(){return categoryDao.selectAll();}
}
