package cn.gary.service;

import cn.gary.models.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.gary.dao.GoodsDao;
import java.util.List;

//商品信息表

@Service(value = "goodsService")
public class GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    public int deleteByPrimaryKey(Integer id){return goodsDao.deleteByPrimaryKey(id);}

    public int insert(Goods goods){return goodsDao.insert(goods);}

    public Goods selectByPrimaryKey(Integer id){return goodsDao.selectByPrimaryKey(id);}

    public int updateByPrimaryKey(Goods goods){return goodsDao.updateByPrimaryKey(goods);}

    public List<Goods> selectAll(){return goodsDao.selectAll();}

    public List<Goods> list(Integer pageIndex, Integer pageSize) {
        return goodsDao.list(pageIndex,pageSize);
    }

    public int count() {
        return goodsDao.count();
    }

    public Goods detail(Integer pid) {
        return new Goods();
    }
}
