package cn.gary.service;

import cn.gary.dao.FlowerDao;
import cn.gary.models.Flower;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//鲜花信息表

@Service(value = "flowerService")
public class FlowerService {
    @Autowired
    private FlowerDao flowerDao;

    public int deleteByPrimaryKey(Integer id){return flowerDao.deleteByPrimaryKey(id);}

    public int insert (Flower flower){return flowerDao.insert(flower);}

    public Flower selectByPrimaryKey(Integer id){return flowerDao.selectByPrimaryKey(id);}

    public int updateByPrimayKey(Flower flower){return flowerDao.updateByPrimaryKey(flower);}

    public List<Flower> selectAll(){return flowerDao.selectAll();}
}
