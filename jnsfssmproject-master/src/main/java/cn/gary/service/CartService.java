package cn.gary.service;

import cn.gary.dao.CartDao;
import cn.gary.models.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//购物车信息表
@Service(value = "cartService")
public class CartService {
    @Autowired
    private CartDao cartDao;

    public int deleteByPrimaryKey(Integer id){return cartDao.deleteByPrimaryKey(id);}

    public int insert(Cart cart){return cartDao.insert(cart);}

    public Cart selectByPrimaryKey(Integer id){return cartDao.selectByPrimaryKey(id);}

    public int updateByPrimaryKey(Cart cart){return cartDao.updateByPrimaryKey(cart);}

    public List<Cart> selectAll(){return cartDao.selectAll();}

    public List<Cart> findByUserId(int userId) {
        return cartDao.selectByUserID(userId);
    }

    public Double totalByUserId(int userId) {
        return cartDao.totalByUserId(userId);
    }
}
