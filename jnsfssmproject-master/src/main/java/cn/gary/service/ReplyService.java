package cn.gary.service;

import cn.gary.dao.ReplyDao;
import cn.gary.models.Reply;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//评价回复评价表

@Service(value = "replyService")
public class ReplyService {

    @Autowired
    private ReplyDao replyDao;

    public int deleteByPrimaryKey(Integer id) {return replyDao.deleteByPrimaryKey(id);}

    public int insert(Reply reply){return replyDao.insert(reply);}

    public Reply selectByPrimaryKey(Integer id){return replyDao.selectByPrimaryKey(id);}

    public int updateByPrimaryKey(Reply reply){return replyDao.updateByPrimaryKey(reply);}

    public List<Reply> selectAll(){return replyDao.selectAll();}
}
