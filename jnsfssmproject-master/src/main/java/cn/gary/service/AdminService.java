package cn.gary.service;

import cn.gary.dao.AdminDao;
import cn.gary.models.Admins;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "adminService")
public class AdminService {

    @Autowired
    private AdminDao adminDao;

    public Admins login(String name, String password) {
        return adminDao.login(name,password);
    }
}
