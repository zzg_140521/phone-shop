package cn.gary.service;

import cn.gary.dao.UserDao;
import cn.gary.models.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//用户信息表

@Service(value = "usersService")
public class UsersService {
//自动装配
    @Autowired
    private UserDao userDao;

    //登录服务
    public boolean login(String username, String userpwd) {
        int result = userDao.login(username, userpwd);
        return (result > 0) ? true : false;
    }

    //用户计数
    public int count() {
        return userDao.count();
    }

    //通过用户名查询
    public Users findByUserName(String username) {
        Users user = userDao.findByUserName(username);
        return user;
    }

    //分页查询
    public List<Users> findPage(Integer pageIndex, Integer pageSize) {
        Integer offset = (pageIndex - 1) * pageSize;
        List<Users> users = userDao.findPage(offset, pageSize);
        return users;
    }

//根据主键删除
    public int deleteByPrimaryKey(Integer id) { return userDao.deleteByPrimaryKey(id);}
//插入
    public int insert(Users users) {return userDao.insert(users);}
//根据主键查询
    public Users selectByPrimaryKey(Integer id){return userDao.selectByPrimaryKey(id);}
//根据主键更改
    public int updateByPrimaryKey(Users users){return userDao.updateByPrimaryKey(users);}
//查询所有
    public List<Users> selectAll(){return userDao.selectAll();}


}
