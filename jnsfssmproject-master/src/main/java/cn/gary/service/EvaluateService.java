package cn.gary.service;

import cn.gary.dao.EvaluateDao;
import cn.gary.models.Evaluate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//评价信息表
@Service(value = "evaluateService")
public class EvaluateService {
    @Autowired
    private EvaluateDao evaluateDao;

    public int deleteByPrimaryKey(Integer id){return evaluateDao.deleteByPrimaryKey(id);}

    public int insert(Evaluate evaluate){return evaluateDao.insert(evaluate);}

    public Evaluate selectByPrimaryKey(Integer id){return evaluateDao.selectByPrimaryKey(id);}

    public int updateByPrimaryKey(Evaluate evaluate){return evaluateDao.updateByPrimaryKey(evaluate);}

    public List<Evaluate> selectAll(){return evaluateDao.selectAll();}
}
