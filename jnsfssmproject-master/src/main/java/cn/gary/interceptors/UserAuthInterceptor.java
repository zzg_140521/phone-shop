package cn.gary.interceptors;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;

public class UserAuthInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //判断用户是否合法
        HttpSession session = request.getSession();
        if (session.getAttribute("username") == null) {

            //指定编译时的字符集
            response.setCharacterEncoding("utf-8");
            //通知客户端浏览器，发给你的是什么东西
            response.setContentType("text/html;charset=utf-8");
            String scriptinfo = "<script>alert(\"未登录或登录超时，请重新登录...\");window.location.href='/index';</script>";
            //将数据直接发送到客户端浏览器
            PrintWriter out = response.getWriter();
            out.print(scriptinfo);

            return false;
        } else {
            return true;
        }

    }
}
