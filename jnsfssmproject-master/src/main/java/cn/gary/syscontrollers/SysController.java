package cn.gary.syscontrollers;

import cn.gary.models.Admins;
import cn.gary.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 后台框架
 */
@Controller
@RequestMapping("/sys")
public class SysController {


    @Autowired
    AdminService adminService;

    //显示登录界面
    @GetMapping("/index")
    public String index(){
        //加载视图
        return "sys/index";
    }
    //接收登录数据，完成登录验证
    @PostMapping("/index")
    public String index(String name, String password, HttpSession session, HttpServletRequest request, Model model){
        //接收数据、验证用户名、密码合法性（访问数据库）
        Admins admin = adminService.login(name, password);

        //if("admin".equals(username) && "admin888".equals(userpwd)){
        if(admin!=null){
            //登录验证成功，应记录用户状态
            session.setAttribute("adminname", name);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日 hh:mm:ss SSS");
            String strDate =dateFormat.format(new Date());
            session.setAttribute("adminlogintime", strDate);
            session.setAttribute("adminloginip", request.getRemoteAddr());

            //转往框架页
            return "redirect:frame";
        }else{
            //验证失败
            //再次显示登录页，并提示原因
            String info = "用户名或密码输入失败，请重试...";
            model.addAttribute("logininfo", info);
            return "sys/index";
        }
    }

    @GetMapping("/frame")
    public String frame(HttpSession session, ModelAndView mav, HttpServletResponse response) throws IOException {
        if(session.getAttribute("adminname")!=null){
            //已验证身份的用户
            return "sys/frame";
        }else{
            //未验证身份的用户，危险
            //String scriptinfo = "未登录或登录超时，请重新登录...";
            //mav.addObject("scriptinfo", scriptinfo)

            response.setCharacterEncoding("utf-8");
            response.setContentType("text/html;charset='utf-8';");
            String scriptinfo = "<script>alert(\"未登录或登录超时，请重新登录...\");window.location.href='index';</script>";
            //将数据直接发送到客户端浏览器
            response.getWriter().print(scriptinfo);

            //提示其原因
            //return "redirect:index";
            return null;
        }
    }

    @GetMapping("/logout")
    public String logout(HttpSession session){
        //退出清理性工作
        //批量清除
        //session.invalidate();
        session.removeAttribute("adminname");
        session.removeAttribute("adminlogintime");
        session.removeAttribute("adminloginip");

        //跳转至登录页
        return "redirect:index";
    }
}
