package cn.gary.syscontrollers;

import cn.gary.utils.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/")
public class SysImagesController {

    @RequestMapping("/image")
    public String image(String filename, HttpServletResponse response) throws IOException {

        //权限验证 vip 扣钱

        IOUtils.readBinary2Browser("C:\\Users\\Gary\\ProjectDemo\\images\\" + filename, response);
        return null;
    }
}
