package cn.gary.dao;


import cn.gary.models.Reply;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository(value = "replyDao")
@Mapper
public interface ReplyDao {
    int deleteByPrimaryKey(Integer replyId);

    int insert(Reply reply);

    Reply selectByPrimaryKey(Integer replyId);

    int updateByPrimaryKey(Reply reply);

    List<Reply> selectAll();
}
