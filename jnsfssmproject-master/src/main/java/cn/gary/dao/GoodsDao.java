package cn.gary.dao;


import cn.gary.models.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository(value = "goodsDao")
@Mapper
public interface GoodsDao {
    int deleteByPrimaryKey(Integer id);

    int insert(Goods goods);

    Goods selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Goods goods);

    List<Goods> selectAll();

    List<Goods> list(Integer pageIndex, Integer pageSize);

    int count();
}
