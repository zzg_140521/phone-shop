package cn.gary.dao;

import cn.gary.models.Users;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository(value = "userDao")
@Mapper
public interface UserDao {
    int deleteByPrimaryKey(Integer id);

    int insert(Users user);

    Users selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Users user);

    List<Users> selectAll();

    int login(String username, String userpwd);

    int count();

    Users findByUserName(String username);

    List<Users> findPage(Integer offset, Integer pageSize);
}
