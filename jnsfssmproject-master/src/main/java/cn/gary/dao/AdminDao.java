package cn.gary.dao;

import cn.gary.models.Address;
import cn.gary.models.Admins;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value = "adminDao")
@Mapper
public interface AdminDao {
    int deleteByPrimaryKey(Integer id);

    int insert(Admins admins);

    Address selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Admins admins);

    List<Address> selectAll();


    Admins login(String name, String password);
}
