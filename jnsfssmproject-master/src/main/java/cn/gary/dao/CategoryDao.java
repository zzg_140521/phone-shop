package cn.gary.dao;

import cn.gary.models.Category;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository(value = "categoryDao")
@Mapper
public interface CategoryDao {
    int deleteByPrimaryKey(Integer id);

    int insert(Category category);

    Category selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Category category);

    List<Category> selectAll();
}
