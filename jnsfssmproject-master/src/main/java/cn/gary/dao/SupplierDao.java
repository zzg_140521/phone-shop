package cn.gary.dao;

import cn.gary.models.Supplier;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository(value = "supplierDao")
@Mapper
public interface SupplierDao {
    int deleteByPrimaryKey(Integer id);

    int insert(Supplier supplier);

    Supplier selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Supplier supplier);

    List<Supplier> selectAll();
}
