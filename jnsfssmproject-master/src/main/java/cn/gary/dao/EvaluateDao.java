package cn.gary.dao;


import cn.gary.models.Evaluate;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository(value = "evaluateDao")
@Mapper
public interface EvaluateDao {
    int deleteByPrimaryKey(Integer id);

    int insert(Evaluate evaluate);

    Evaluate selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Evaluate evaluate);

    List<Evaluate> selectAll();
}
