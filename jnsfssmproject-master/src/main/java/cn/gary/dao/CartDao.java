package cn.gary.dao;

import cn.gary.models.Cart;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value = "cartDao")
@Mapper
public interface CartDao {
    int deleteByPrimaryKey(Integer id);

    int insert(Cart cart);

    Cart selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Cart cart);

    List<Cart> selectAll();

    Double totalByUserId(int userId);

    List<Cart> selectByUserID(int userId);

    void deleteByUserId(int userId);
}
