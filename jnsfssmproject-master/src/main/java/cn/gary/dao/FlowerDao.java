package cn.gary.dao;


import cn.gary.models.Flower;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository(value = "flowerDao")
@Mapper
public interface FlowerDao {
    int deleteByPrimaryKey(Integer flowerId);

    int insert(Flower flower);

    Flower selectByPrimaryKey(Integer flowerId);

    int updateByPrimaryKey(Flower flower);

    List<Flower> selectAll();
}
