package cn.gary.dao;


import cn.gary.models.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository(value = "ordersDao")
@Mapper
public interface OrdersDao {
    int deleteByPrimaryKey(Integer id);

    int insert(Orders orders);

    Orders selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Orders orders);

    List<Orders> selectAll();

    List<Orders> findByUserId(Integer userId);
}
