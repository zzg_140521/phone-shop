package cn.gary.models;

import java.util.Date;

public class Evaluate {
    private int evaluateId;
    private int userId;
    private int ordersId;
    private int goodsId;
    private String starValue;
    private String content;
    private Date createTime;

    @Override
    public String toString() {
        return "Evaluate{" +
                "evaluateId=" + evaluateId +
                ", userId=" + userId +
                ", ordersId=" + ordersId +
                ", goodsId=" + goodsId +
                ", starValue='" + starValue + '\'' +
                ", content='" + content + '\'' +
                ", createTime=" + createTime +
                '}';
    }

    public Evaluate(int evaluateId, int userId, int ordersId, int goodsId, String starValue, String content, Date createTime) {
        this.evaluateId = evaluateId;
        this.userId = userId;
        this.ordersId = ordersId;
        this.goodsId = goodsId;
        this.starValue = starValue;
        this.content = content;
        this.createTime = createTime;
    }

    public Evaluate() {
    }

    public int getEvaluateId() {
        return evaluateId;
    }

    public void setEvaluateId(int evaluateId) {
        this.evaluateId = evaluateId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getOrdersId() {
        return ordersId;
    }

    public void setOrdersId(int ordersId) {
        this.ordersId = ordersId;
    }

    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    public String getStarValue() {
        return starValue;
    }

    public void setStarValue(String starValue) {
        this.starValue = starValue;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
