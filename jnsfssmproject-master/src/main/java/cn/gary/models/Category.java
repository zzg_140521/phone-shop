package cn.gary.models;

import java.util.Date;

public class Category {
    private int categoryId;
    private String name;
    private Date createTime;

    @Override
    public String toString() {
        return "Category{" +
                "categoryId=" + categoryId +
                ", name='" + name + '\'' +
                ", createTime=" + createTime +
                '}';
    }

    public Category(int categoryId, String name, Date createTime) {
        this.categoryId = categoryId;
        this.name = name;
        this.createTime = createTime;
    }

    public Category() {
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
