package cn.gary.models;


import java.util.Date;

public class Cart {
    private int cartId;
    private int userId;
    private int goodsId;
    private int goodsNum;
    private double price;
    private Date addTime;

    @Override
    public String toString() {
        return "Cart{" +
                "cartId=" + cartId +
                ", userId=" + userId +
                ", goodsId=" + goodsId +
                ", goodsNum=" + goodsNum +
                ", price=" + price +
                ", addTime=" + addTime +
                '}';
    }

    public Cart(int cartId, int userId, int goodsId, int goodsNum, double price, Date addTime) {
        this.cartId = cartId;
        this.userId = userId;
        this.goodsId = goodsId;
        this.goodsNum = goodsNum;
        this.price = price;
        this.addTime = addTime;
    }

    public Cart() {
    }

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    public int getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(int goodsNum) {
        this.goodsNum = goodsNum;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
