package cn.gary.models;

import java.util.Date;

public class Flower {
    private int flowerId;
    private int supplierId;
    private String name;
    private String img;
    private String varieties;
    private double purchasePrice;
    private String placeOfOrigin;
    private String flowerLanguage;
    private Date purchasingTime;
    private int isDelete;

    @Override
    public String toString() {
        return "Flower{" +
                "flowerId=" + flowerId +
                ", supplierId=" + supplierId +
                ", name='" + name + '\'' +
                ", img='" + img + '\'' +
                ", varieties='" + varieties + '\'' +
                ", purchasePrice=" + purchasePrice +
                ", placeOfOrigin='" + placeOfOrigin + '\'' +
                ", flowerLanguage='" + flowerLanguage + '\'' +
                ", purchasingTime=" + purchasingTime +
                ", isDelete=" + isDelete +
                '}';
    }

    public Flower(int flowerId, int supplierId, String name, String img, String varieties, double purchasePrice, String placeOfOrigin, String flowerLanguage, Date purchasingTime, int isDelete) {
        this.flowerId = flowerId;
        this.supplierId = supplierId;
        this.name = name;
        this.img = img;
        this.varieties = varieties;
        this.purchasePrice = purchasePrice;
        this.placeOfOrigin = placeOfOrigin;
        this.flowerLanguage = flowerLanguage;
        this.purchasingTime = purchasingTime;
        this.isDelete = isDelete;
    }

    public Flower() {
    }

    public int getFlowerId() {
        return flowerId;
    }

    public void setFlowerId(int flowerId) {
        this.flowerId = flowerId;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getVarieties() {
        return varieties;
    }

    public void setVarieties(String varieties) {
        this.varieties = varieties;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getPlaceOfOrigin() {
        return placeOfOrigin;
    }

    public void setPlaceOfOrigin(String placeOfOrigin) {
        this.placeOfOrigin = placeOfOrigin;
    }

    public String getFlowerLanguage() {
        return flowerLanguage;
    }

    public void setFlowerLanguage(String flowerLanguage) {
        this.flowerLanguage = flowerLanguage;
    }

    public Date getPurchasingTime() {
        return purchasingTime;
    }

    public void setPurchasingTime(Date purchasingTime) {
        this.purchasingTime = purchasingTime;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }
}
