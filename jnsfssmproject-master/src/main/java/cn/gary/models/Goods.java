package cn.gary.models;

public class Goods {
    private int goodsId;
    private int categoryId;
    private String title;
    private String img;
    private String introduce;

    @Override
    public String toString() {
        return "Goods{" +
                "goodsId=" + goodsId +
                ", categoryId=" + categoryId +
                ", title='" + title + '\'' +
                ", img='" + img + '\'' +
                ", introduce='" + introduce + '\'' +
                ", sales=" + sales +
                ", price=" + price +
                ", flowerId=" + flowerId +
                ", placeOfOrigin='" + placeOfOrigin + '\'' +
                ", flowerLanguage=" + flowerLanguage +
                ", isShelves=" + isShelves +
                ", isSales=" + isSales +
                ", isHot=" + isHot +
                ", isRecommended=" + isRecommended +
                ", isDelete=" + isDelete +
                '}';
    }

    public Goods() {
    }

    private int sales;
    private double price;
    private int flowerId;
    private String placeOfOrigin;
    private int flowerLanguage;
    private int isShelves;
    private int isSales;

    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getFlowerId() {
        return flowerId;
    }

    public void setFlowerId(int flowerId) {
        this.flowerId = flowerId;
    }

    public String getPlaceOfOrigin() {
        return placeOfOrigin;
    }

    public void setPlaceOfOrigin(String placeOfOrigin) {
        this.placeOfOrigin = placeOfOrigin;
    }

    public int getFlowerLanguage() {
        return flowerLanguage;
    }

    public void setFlowerLanguage(int flowerLanguage) {
        this.flowerLanguage = flowerLanguage;
    }

    public int getIsShelves() {
        return isShelves;
    }

    public void setIsShelves(int isShelves) {
        this.isShelves = isShelves;
    }

    public int getIsSales() {
        return isSales;
    }

    public void setIsSales(int isSales) {
        this.isSales = isSales;
    }

    public int getIsHot() {
        return isHot;
    }

    public void setIsHot(int isHot) {
        this.isHot = isHot;
    }

    public int getIsRecommended() {
        return isRecommended;
    }

    public void setIsRecommended(int isRecommended) {
        this.isRecommended = isRecommended;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    private int isHot;
    private int isRecommended;
    private int isDelete;
}
