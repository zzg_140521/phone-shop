package cn.gary.models;

import java.util.Date;

public class Orders {
    private int ordersId;
    private int goodsId;
    private int userId;
    private int totalPrice;
    private int goodsPrice;
    private int goodsNum;
    private int preferential;
    private int dealPrice;
    private String note;
    private Date createTime;
    private Date paymentTime;
    private Date completeTime;
    private int isCancel;

    @Override
    public String toString() {
        return "Orders{" +
                "ordersId=" + ordersId +
                ", goodsId=" + goodsId +
                ", userId=" + userId +
                ", totalPrice=" + totalPrice +
                ", goodsPrice=" + goodsPrice +
                ", goodsNum=" + goodsNum +
                ", preferential=" + preferential +
                ", dealPrice=" + dealPrice +
                ", note='" + note + '\'' +
                ", createTime=" + createTime +
                ", paymentTime=" + paymentTime +
                ", completeTime=" + completeTime +
                ", isCancel=" + isCancel +
                '}';
    }

    public Orders() {
    }

    public int getOrdersId() {
        return ordersId;
    }

    public void setOrdersId(int ordersId) {
        this.ordersId = ordersId;
    }

    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(int goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public int getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(int goodsNum) {
        this.goodsNum = goodsNum;
    }

    public int getPreferential() {
        return preferential;
    }

    public void setPreferential(int preferential) {
        this.preferential = preferential;
    }

    public int getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(int dealPrice) {
        this.dealPrice = dealPrice;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }

    public Date getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Date completeTime) {
        this.completeTime = completeTime;
    }

    public int getIsCancel() {
        return isCancel;
    }

    public void setIsCancel(int isCancel) {
        this.isCancel = isCancel;
    }
}
