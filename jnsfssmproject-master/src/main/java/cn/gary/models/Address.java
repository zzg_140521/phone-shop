package cn.gary.models;

public class Address {
    private int addressId;
    private int userId;
    private String province;
    private String city;
    private String area;
    private String street;
    private int isDefault;

    public Address() {
    }

    public Address(int addressId, int userId, String province, String city, String area, String street, int isDefault) {
        this.addressId = addressId;
        this.userId = userId;
        this.province = province;
        this.city = city;
        this.area = area;
        this.street = street;
        this.isDefault = isDefault;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(int isDefault) {
        this.isDefault = isDefault;
    }
}
