package cn.gary.models;

import java.util.Date;

public class Reply {
    private int replyId;
    private int evaluateId;
    private int empId;
    private String content;
    private Date createTime;

    @Override
    public String toString() {
        return "Reply{" +
                "replyId=" + replyId +
                ", evaluateId=" + evaluateId +
                ", empId=" + empId +
                ", content='" + content + '\'' +
                ", createTime=" + createTime +
                '}';
    }

    public Reply() {
    }

    public int getReplyId() {
        return replyId;
    }

    public void setReplyId(int replyId) {
        this.replyId = replyId;
    }

    public int getEvaluateId() {
        return evaluateId;
    }

    public void setEvaluateId(int evaluateId) {
        this.evaluateId = evaluateId;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
