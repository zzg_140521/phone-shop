/*
Navicat MySQL Data Transfer

Source Server         : Mysql5.6ByLinux
Source Server Version : 50718
Source Host           : 192.168.150.243:3306
Source Database       : shoppingdb

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2021-09-16 13:42:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for T_Orders_Cart
-- ----------------------------
DROP TABLE IF EXISTS `T_Orders_Cart`;
CREATE TABLE `T_Orders_Cart` (
  `CartId` int(11) NOT NULL AUTO_INCREMENT,
  `ProductId` int(11) DEFAULT '0',
  `ProductName` varchar(50) DEFAULT NULL,
  `ProductPrice` float(8,2) DEFAULT '0.00',
  `CartAmount` int(11) DEFAULT '0',
  `UserId` int(11) DEFAULT NULL,
  PRIMARY KEY (`CartId`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_Orders_Cart
-- ----------------------------
INSERT INTO `T_Orders_Cart` VALUES ('1', '1', 'Mate10', '100.00', '5', null);
INSERT INTO `T_Orders_Cart` VALUES ('2', '2', '泳衣机', '500.00', '5', null);
INSERT INTO `T_Orders_Cart` VALUES ('3', '2', '泳衣机', '500.00', '5', null);
INSERT INTO `T_Orders_Cart` VALUES ('4', '2', '泳衣机', '500.00', '5', null);
INSERT INTO `T_Orders_Cart` VALUES ('5', '2', '泳衣机', '500.00', '5', null);
INSERT INTO `T_Orders_Cart` VALUES ('6', '1', 'xxx', '1.10', '1', '1');
INSERT INTO `T_Orders_Cart` VALUES ('7', '1', 'xxx', '1.10', '1', '1');
INSERT INTO `T_Orders_Cart` VALUES ('8', '1', 'xxx', '1.10', '1', '1');
INSERT INTO `T_Orders_Cart` VALUES ('9', '1', 'xxx', '1.10', '1', '1');
INSERT INTO `T_Orders_Cart` VALUES ('10', null, null, null, null, null);
INSERT INTO `T_Orders_Cart` VALUES ('13', '1', 'xxx', '1.10', '1', '1');
INSERT INTO `T_Orders_Cart` VALUES ('14', '1', 'xxx', '1.10', '1', '1');
INSERT INTO `T_Orders_Cart` VALUES ('15', null, null, null, null, null);

-- ----------------------------
-- Table structure for T_Orders_OrderItems
-- ----------------------------
DROP TABLE IF EXISTS `T_Orders_OrderItems`;
CREATE TABLE `T_Orders_OrderItems` (
  `OrderItemId` int(11) NOT NULL AUTO_INCREMENT,
  `OrderId` varchar(20) DEFAULT '0',
  `OrderItemProductId` int(11) DEFAULT '0',
  `OrderItemProductName` varchar(50) DEFAULT NULL,
  `OrderItemProductMemberPrice` float(8,2) DEFAULT '0.00',
  `OrderItemAmount` int(11) DEFAULT '0',
  `OrderItemProductPrice` float(8,2) DEFAULT '0.00',
  PRIMARY KEY (`OrderItemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_Orders_OrderItems
-- ----------------------------

-- ----------------------------
-- Table structure for T_Orders_Orders
-- ----------------------------
DROP TABLE IF EXISTS `T_Orders_Orders`;
CREATE TABLE `T_Orders_Orders` (
  `OrderId` varchar(20) NOT NULL,
  `OrderSumPrice` float(8,2) DEFAULT '0.00',
  `OrderDeliverName` varchar(30) DEFAULT NULL,
  `OrderDeliverAddress` varchar(50) DEFAULT NULL,
  `OrderDeliverZipCode` varchar(10) DEFAULT NULL,
  `OrderDeliverMobile` varchar(15) DEFAULT NULL,
  `DeliverId` int(11) DEFAULT '0',
  `PaymentName` int(11) DEFAULT NULL COMMENT '（在线支付, 银行汇款/邮局汇款, 货到付款）',
  `OrderConsignmentNumber` varchar(50) DEFAULT NULL,
  `OrderDescribe` longblob,
  `OrderState` char(1) DEFAULT '0' COMMENT '订单处理中, 订单取消（客户操作的）, 订单作废（管理员操作的）, 订单完成，订单退货；',
  `OrderDeleted` char(1) DEFAULT NULL,
  `UserId` int(11) DEFAULT '0',
  `UserLoginName` varchar(30) DEFAULT NULL,
  `OrderBookedDate` datetime DEFAULT NULL,
  `OrderModifyDate` datetime DEFAULT NULL,
  PRIMARY KEY (`OrderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_Orders_Orders
-- ----------------------------

-- ----------------------------
-- Table structure for T_Orders_Rate
-- ----------------------------
DROP TABLE IF EXISTS `T_Orders_Rate`;
CREATE TABLE `T_Orders_Rate` (
  `rateId` int(11) NOT NULL AUTO_INCREMENT,
  `OrderId` varchar(20) DEFAULT NULL,
  `UserId` int(11) DEFAULT NULL,
  `stars` varchar(50) DEFAULT NULL,
  `rateDate` char(10) DEFAULT NULL,
  PRIMARY KEY (`rateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_Orders_Rate
-- ----------------------------

-- ----------------------------
-- Table structure for T_Shopping_ProClass
-- ----------------------------
DROP TABLE IF EXISTS `T_Shopping_ProClass`;
CREATE TABLE `T_Shopping_ProClass` (
  `ClassId` int(11) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ClassId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_Shopping_ProClass
-- ----------------------------
INSERT INTO `T_Shopping_ProClass` VALUES ('1', '华为', null);
INSERT INTO `T_Shopping_ProClass` VALUES ('2', 'OPPO', null);

-- ----------------------------
-- Table structure for T_Shopping_Products
-- ----------------------------
DROP TABLE IF EXISTS `T_Shopping_Products`;
CREATE TABLE `T_Shopping_Products` (
  `ProductId` int(11) NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(50) DEFAULT NULL,
  `ProductClassId` char(10) DEFAULT NULL,
  `ProductSmallPhoto` varchar(200) DEFAULT NULL,
  `ProductBigPhoto` varchar(200) DEFAULT NULL,
  `ProductSummary` varchar(200) DEFAULT NULL,
  `ProductDescribe` longblob,
  `ProductMarketPrice` float(8,2) DEFAULT '0.00',
  `ProductMemberPrice` float(8,2) DEFAULT '0.00',
  `ProductStock` int(11) DEFAULT '0',
  `ProductBuyTimes` int(11) DEFAULT '0',
  `ProductHits` int(11) DEFAULT '0',
  PRIMARY KEY (`ProductId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_Shopping_Products
-- ----------------------------
INSERT INTO `T_Shopping_Products` VALUES ('1', 'Mate10', '1', null, null, null, null, '100.00', '100.00', '0', '0', '0');
INSERT INTO `T_Shopping_Products` VALUES ('2', '泳衣机', '2', null, null, null, null, '500.00', '500.00', '0', '0', '0');

-- ----------------------------
-- Table structure for T_Sys_Administrators
-- ----------------------------
DROP TABLE IF EXISTS `T_Sys_Administrators`;
CREATE TABLE `T_Sys_Administrators` (
  `adminId` int(11) NOT NULL AUTO_INCREMENT,
  `deptId` int(11) DEFAULT NULL,
  `adminName` varchar(20) DEFAULT NULL,
  `loginName` varchar(20) DEFAULT NULL,
  `loginPwd` varchar(20) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `tel` varchar(15) DEFAULT NULL,
  `registeDate` datetime DEFAULT NULL,
  PRIMARY KEY (`adminId`),
  KEY `FK_Reference_1` (`deptId`),
  CONSTRAINT `FK_Reference_1` FOREIGN KEY (`deptId`) REFERENCES `T_Sys_Departments` (`deptId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_Sys_Administrators
-- ----------------------------
INSERT INTO `T_Sys_Administrators` VALUES ('1', '1', '苏关兴', 'gary', 'dragon', '0', '13522427089', '2020-03-03 00:00:00');
INSERT INTO `T_Sys_Administrators` VALUES ('2', '1', '超级管理员', 'admin', 'admin888', '0', '1888888888', '2020-05-17 09:06:49');

-- ----------------------------
-- Table structure for T_Sys_Departments
-- ----------------------------
DROP TABLE IF EXISTS `T_Sys_Departments`;
CREATE TABLE `T_Sys_Departments` (
  `deptId` int(11) NOT NULL AUTO_INCREMENT,
  `deptName` varchar(50) DEFAULT NULL,
  `deptNum` int(11) DEFAULT NULL,
  PRIMARY KEY (`deptId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_Sys_Departments
-- ----------------------------
INSERT INTO `T_Sys_Departments` VALUES ('1', '技术实施部', '28');
INSERT INTO `T_Sys_Departments` VALUES ('2', '院校合作部', '16');

-- ----------------------------
-- Table structure for T_User_Courier
-- ----------------------------
DROP TABLE IF EXISTS `T_User_Courier`;
CREATE TABLE `T_User_Courier` (
  `CourierId` int(11) NOT NULL AUTO_INCREMENT,
  `OrderId` varchar(20) DEFAULT NULL,
  `CourierName` varchar(20) DEFAULT NULL,
  `CourierPicture` varchar(40) DEFAULT NULL,
  `CourierPhone` varchar(20) DEFAULT NULL,
  `CourierAddress` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`CourierId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_User_Courier
-- ----------------------------

-- ----------------------------
-- Table structure for T_User_FeedBack
-- ----------------------------
DROP TABLE IF EXISTS `T_User_FeedBack`;
CREATE TABLE `T_User_FeedBack` (
  `FeedBackId` int(11) NOT NULL AUTO_INCREMENT,
  `FeedBackInfor` varchar(50) DEFAULT NULL,
  `FeedBackTimes` datetime DEFAULT NULL,
  `UserId` int(11) DEFAULT NULL,
  PRIMARY KEY (`FeedBackId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_User_FeedBack
-- ----------------------------

-- ----------------------------
-- Table structure for T_User_Manager
-- ----------------------------
DROP TABLE IF EXISTS `T_User_Manager`;
CREATE TABLE `T_User_Manager` (
  `ManagerId` int(11) NOT NULL,
  `ManagerName` varchar(20) DEFAULT NULL,
  `ManagerPwd` varchar(20) DEFAULT NULL,
  `RightId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ManagerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_User_Manager
-- ----------------------------

-- ----------------------------
-- Table structure for T_User_News
-- ----------------------------
DROP TABLE IF EXISTS `T_User_News`;
CREATE TABLE `T_User_News` (
  `NewsId` int(11) NOT NULL AUTO_INCREMENT,
  `NewsContent` varchar(30) DEFAULT NULL,
  `NewsDateTime` datetime DEFAULT NULL,
  `NewsPublisher` varchar(20) DEFAULT NULL,
  `NewsTitle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`NewsId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_User_News
-- ----------------------------

-- ----------------------------
-- Table structure for T_User_Order
-- ----------------------------
DROP TABLE IF EXISTS `T_User_Order`;
CREATE TABLE `T_User_Order` (
  `OrderId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) DEFAULT NULL,
  `FoodsId` int(11) DEFAULT NULL,
  `OrderDatetime` datetime DEFAULT NULL,
  `ShopId` int(11) DEFAULT NULL,
  `OrderState` varchar(20) DEFAULT NULL,
  `OrderPirce` decimal(8,0) DEFAULT NULL,
  `OrderTotal` decimal(8,0) DEFAULT NULL,
  PRIMARY KEY (`OrderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_User_Order
-- ----------------------------

-- ----------------------------
-- Table structure for T_User_Orderdetails
-- ----------------------------
DROP TABLE IF EXISTS `T_User_Orderdetails`;
CREATE TABLE `T_User_Orderdetails` (
  `OrderdetailsId` int(11) NOT NULL AUTO_INCREMENT,
  `T_U_OrderId` varchar(20) DEFAULT NULL,
  `OrderdetailsPirce` decimal(8,0) DEFAULT NULL,
  `OrderdetailsCount` int(11) DEFAULT NULL,
  `OrderdetailsContent` varchar(60) DEFAULT NULL,
  `OrderdetailsTimes` datetime DEFAULT NULL,
  `OrderdetailsPicture` varchar(50) DEFAULT NULL,
  `OrderId` int(11) DEFAULT NULL,
  PRIMARY KEY (`OrderdetailsId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_User_Orderdetails
-- ----------------------------

-- ----------------------------
-- Table structure for T_User_Products
-- ----------------------------
DROP TABLE IF EXISTS `T_User_Products`;
CREATE TABLE `T_User_Products` (
  `FoodsId` int(11) NOT NULL AUTO_INCREMENT,
  `CTypesId` int(11) DEFAULT NULL,
  `FoodsName` varchar(20) DEFAULT NULL,
  `FoodsPictrue` varchar(50) DEFAULT NULL,
  `FoodsPrice` decimal(8,0) DEFAULT NULL,
  `FoodsIntroduce` varchar(50) DEFAULT NULL,
  `RecommendCai` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`FoodsId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_User_Products
-- ----------------------------

-- ----------------------------
-- Table structure for T_User_Right
-- ----------------------------
DROP TABLE IF EXISTS `T_User_Right`;
CREATE TABLE `T_User_Right` (
  `RightId` int(11) NOT NULL,
  `RightName` varchar(50) DEFAULT NULL,
  `RoleContent` varchar(50) DEFAULT NULL,
  `RightState` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`RightId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_User_Right
-- ----------------------------

-- ----------------------------
-- Table structure for T_User_Shopping
-- ----------------------------
DROP TABLE IF EXISTS `T_User_Shopping`;
CREATE TABLE `T_User_Shopping` (
  `ShoppingId` int(11) NOT NULL AUTO_INCREMENT,
  `FoodsId` int(11) DEFAULT NULL,
  `UserId` int(11) DEFAULT NULL,
  `ShoppingName` varchar(20) DEFAULT NULL,
  `ShoppingTimess` datetime DEFAULT NULL,
  `ShoppingPirce` decimal(8,0) DEFAULT NULL,
  `ShoppingCount` int(11) DEFAULT NULL,
  PRIMARY KEY (`ShoppingId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_User_Shopping
-- ----------------------------

-- ----------------------------
-- Table structure for T_User_Types
-- ----------------------------
DROP TABLE IF EXISTS `T_User_Types`;
CREATE TABLE `T_User_Types` (
  `CTypesId` int(11) NOT NULL AUTO_INCREMENT,
  `CTypesName` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`CTypesId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_User_Types
-- ----------------------------
INSERT INTO `T_User_Types` VALUES ('1', '普通会员');
INSERT INTO `T_User_Types` VALUES ('2', '黄金会员');

-- ----------------------------
-- Table structure for T_User_User
-- ----------------------------
DROP TABLE IF EXISTS `T_User_User`;
CREATE TABLE `T_User_User` (
  `UserId` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(20) DEFAULT NULL,
  `UserPwd` varchar(20) DEFAULT NULL,
  `UserPhone` varchar(13) DEFAULT NULL,
  `UserPirce` decimal(8,0) DEFAULT NULL,
  `UserSex` int(11) DEFAULT NULL,
  `UserPicture` varchar(50) DEFAULT NULL,
  `UserBirthday` datetime DEFAULT NULL,
  `UserAddress` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`UserId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of T_User_User
-- ----------------------------
INSERT INTO `T_User_User` VALUES ('1', 'gary', 'dragon', '13522427089', null, '0', null, null, null);
INSERT INTO `T_User_User` VALUES ('3', 'tony', '23423', '342', '234', '342', null, null, null);
INSERT INTO `T_User_User` VALUES ('4', 'grace', 'sdfkl', null, null, '0', null, null, null);
INSERT INTO `T_User_User` VALUES ('7', 'gary1', 'dragon', '', null, null, null, null, '');
INSERT INTO `T_User_User` VALUES ('8', '', '', '', null, null, null, null, '');
INSERT INTO `T_User_User` VALUES ('9', 'gary324324', 'dragon', '', null, null, 'b82e14c0dc514d01953ba7c891de9ecf.jpg', null, '');

-- ----------------------------
-- Procedure structure for p_GenerateUserOrder
-- ----------------------------
DROP PROCEDURE IF EXISTS `p_GenerateUserOrder`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `p_GenerateUserOrder`(IN `userid` int)
BEGIN
	#Routine body goes here...
END
;;
DELIMITER ;
